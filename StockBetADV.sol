pragma solidity ^0.4.4;

contract StockBetADV{
	struct Better {
	    string name;
	    address betteraddress;
        uint useramount;
        string stockname; 
        uint stockpriceChangepercent;
    }
    struct Bet {
	    uint id;
        uint startTime;
	    uint endTime;
	    Better[] betterList;
	    uint totalBetAmount;
	    string winningstock;
	    Better winner;
    }
    
    //Varibales
    mapping(uint => Bet) betList;
     string winnername;
     Better actualWinner;
    
    //Events
    event LogBetOnStock(string name,uint amount,string stockName);
    event BetInitialized(uint betId,string stockName, uint startTimeInMinutes, uint endTimeInMinutes,string yourName);
    
    function StockBetADV() {
    }
    
    function initilizeBet(uint betId,string stockNameUser, uint startTimeInMinutes, uint endTimeInMinutes,string yourName,uint guessStockprice) payable{
        betList[betId].id=betId;
        betList[betId].startTime= now + startTimeInMinutes * 1 minutes;
        betList[betId].endTime=now + endTimeInMinutes * 1 minutes;
        betList[betId].totalBetAmount=msg.value;
        Better[] betterListuser= betList[betId].betterList;
        
            betterListuser.push(Better({
                name: yourName,
                betteraddress: msg.sender,
                useramount:msg.value,
                stockpriceChangepercent:guessStockprice,
                stockname:stockNameUser
            }));
        BetInitialized(betId,stockNameUser,startTimeInMinutes,endTimeInMinutes,yourName);
    }
    
    function betOnStock(uint betId,string betterName,uint guessStockprice, string stocknameuser) payable{
        if(now<betList[betId].endTime){
            Better[] betterListuser= betList[betId].betterList;
                betterListuser.push(Better({
                    name: betterName,
                    betteraddress: msg.sender,
                    useramount:msg.value,
                    stockpriceChangepercent:guessStockprice,
                    stockname:stocknameuser
                }));
            
            betList[betId].betterList=betterListuser;
            betList[betId].totalBetAmount +=msg.value;
            LogBetOnStock(betterName, msg.value,stocknameuser);
        }
    }
   
     function declareResult(uint betId) returns (string){
         uint endtime=betList[betId].endTime;
         uint totalbetamountwon=betList[betId].totalBetAmount;
         uint winningStockPrice;
         bool returnMoney=false;
        
         
         if(now>endtime){
             Better[] betterListuser= betList[betId].betterList;
             for (uint i = 0; i < betterListuser.length; i ++){
                    uint currentStockPrice=betterListuser[i].stockpriceChangepercent;//must come form webservice 
                    if(currentStockPrice>winningStockPrice){
                        winningStockPrice=currentStockPrice;
                        actualWinner=betterListuser[i];
                    }
                    if(currentStockPrice==winningStockPrice){
                        returnMoney=true;
                    }
            }
            return sentMoneyToUser(betId, returnMoney,totalbetamountwon);
          
         }
         return winnername;
     }

    function sentMoneyToUser(uint betId,bool returnMoney,uint totalbetamountwon) internal returns(string winnername){
          Better[] betterListuser= betList[betId].betterList;
         if(returnMoney){
               for (uint j = 0; j < betterListuser.length; j ++){
                 bool sent=betterListuser[j].betteraddress.send(betterListuser[j].useramount);
               }
               winnername="No Winner";
           }else if(actualWinner.betteraddress.send(totalbetamountwon)){
               winnername=actualWinner.name;           
           }
    }
   /* 
    function stringsEqual(string storage _a, string memory _b) internal returns (bool) {
		bytes storage a = bytes(_a);
		bytes memory b = bytes(_b);
		if (a.length != b.length)
			return false;
		// @todo unroll this loop
		for (uint i = 0; i < a.length; i ++)
			if (a[i] != b[i])
				return false;
		return true;
	}*/
   
}