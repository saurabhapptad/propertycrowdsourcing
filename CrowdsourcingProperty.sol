pragma solidity ^0.4.2;


contract CrowdsourcingProperty {
    
    struct Invester{
        address inversteraddress;
        uint inversteramount;
        string email;
        string name;
		uint remaingAmount;
    }
    
   
    uint public fundingGoal; 
    uint public amountRaised; 
    uint public deadline; 
    Invester[] investerList;
    
    
    bool fundingGoalReached = false;
    bool crowdsaleClosed = false;
    
    event GoalReached(uint amountRaised);
    event FundTransfer(address backer, uint amount, bool isContribution);
	event PayInsvestor(uint amount);
	event PaymentDone(string s);
	event PaymentNotDone(string s);
    

    function CrowdsourcingProperty(uint fundingGoalInEthers,uint durationInMinutes) {
        fundingGoal = fundingGoalInEthers * 1 ether;
        deadline = now + durationInMinutes * 1 minutes;
    }

    
    function investOnProperty(string yourName, string youremail) payable {
        if (crowdsaleClosed) throw;
        if (now >= deadline) throw;
        
        investerList.push(Invester({
                name: yourName,
                email: youremail,
                inversteraddress: msg.sender,
                inversteramount:msg.value,
				remaingAmount:0
            }));
       
        amountRaised += msg.value;
        FundTransfer(msg.sender, msg.value, true);
    }

    modifier afterDeadline() { if (now >= deadline) _; }
    function checkGoalReached(address othercontractaddress) afterDeadline {
        if (amountRaised > fundingGoal){
            fundingGoalReached = true;
            GoalReached(amountRaised);
            uint extraMoney=amountRaised-fundingGoal;
            if(othercontractaddress.send(extraMoney)){
                amountRaised=fundingGoal;
            }
        }
        else if (amountRaised == fundingGoal){
            fundingGoalReached = true;
            GoalReached(amountRaised);
        }else{
             for (uint i = 0; i < investerList.length; i++){
                 uint amount1= investerList[i].remaingAmount;
                 if(amount1<amountRaised){
                     if(investerList[i].inversteraddress.send(amount1)){
						PaymentDone('Payemt is given to Investor');
                         amountRaised -=amount1;
                         investerList[i].remaingAmount=0;
                     }
                 }
                   
             }
        }
        crowdsaleClosed = true;
    }


    function payInverstor(uint percent) {
         for (uint i = 0; i < investerList.length; i++){
                 uint amount= investerList[i].inversteramount;
                 uint percentamount=amount * percent/100;				
                 if(percentamount<=amountRaised){
						PayInsvestor(percentamount);
						amountRaised -=percentamount;
						investerList[i].remaingAmount -=percentamount;
                     if(investerList[i].inversteraddress.send(percentamount)){
                         PaymentDone('Payemt is given to Investor');
                     }else {
						PaymentNotDone('aaa');
						amountRaised +=percentamount;
						investerList[i].remaingAmount +=percentamount;
					 }
                 }
                   
             }
    }
}